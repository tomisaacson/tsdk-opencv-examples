/*
 *  stereo_match.cpp
 *  calibration
 *
 *  Created by Victor  Eruhimov on 1/18/10.
 *  Copyright 2010 Argus Corp. All rights reserved.
 *
 */

#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/core/utility.hpp"

#include <stdio.h>

using namespace cv;


static void print_help()
{
    printf("\nDemo stereo matching converting L and R images into disparity and point clouds\n");
    printf("\nUsage: [--algorithm=_pBM|_pSGBM|hh|sgbm3way] [--blocksize=<block_size>]\n"
           "[--max-disparity=<max_disparity>] [--min-disparity=<min_disparity>] [speckle-window-size=] [speckle-range=]\n"
           "[-i=<intrinsic_filename>] [-e=<extrinsic_filename>]\n"
           "[-o=<disparity_image>] [-p=<point_cloud_file>]\n");
}


class Disparity {
    enum class eStereo { UNKNOWN, BM, SGBM, HH, VAR, THREEWAY };

    private:       
        const cv::Size _img_size;
        std::string _intrinsic_filename;
        std::string _extrinsic_filename;
        std::string _disparity_filename;
        std::string _point_cloud_filename;
        eStereo _alg;
        int _SADWindowSize, _numberOfDisparities;
        int _minDisparity;
        int _speckleWindowSize;
        int _speckleRange;
        Rect _roi1, _roi2;
        Mat _map11, _map12, _map21, _map22, _Q;
        Ptr<StereoBM> _pBM;
        Ptr<StereoSGBM> _pSGBM;

    public:
        Disparity(cv::Size, cv::CommandLineParser &);
        bool init();
        bool show_disparity();

        void set_minDisp(int minDisp) { _pSGBM->setMinDisparity(minDisp); }
        void set_numDisp(int numDisp) { _pSGBM->setNumDisparities(numDisp); }
        void set_blocksize(int blocksize) { _pSGBM->setBlockSize(blocksize); }
        void set_speckleWindowSize(int speckle_window_size) { _pSGBM->setSpeckleWindowSize(speckle_window_size); }
        void set_speckleRange(int speckle_range) { _pSGBM->setSpeckleRange(speckle_range); }
};


void on_minDisp(int min_disp, void *disp_ptr)
{
    Disparity *disp_obj = static_cast<Disparity *>(disp_ptr);
    printf("minDisparity = %d\n", min_disp);
    disp_obj->set_minDisp(min_disp /*- 30*/);
}


void on_numDisp(int num_disp, void *disp_ptr)
{
    Disparity *disp_obj = static_cast<Disparity *>(disp_ptr);
    num_disp = (num_disp / 16) * 16;
    printf("numDisparity = %d\n", num_disp);
    setTrackbarPos("numDisparity", "Disparity", num_disp);
    disp_obj->set_numDisp(num_disp);
}


void on_blocksize(int blocksize, void *disp_ptr)
{
    Disparity *disp_obj = static_cast<Disparity *>(disp_ptr);
    if (blocksize < 1)
        blocksize = 1;
    else if (blocksize % 2 != 1)
        blocksize -= 1;
    printf("blocksize = %d\n", blocksize);
    setTrackbarPos("blocksize", "Disparity", blocksize);
    disp_obj->set_blocksize(blocksize);
}


void on_speckleWindowSize(int speckle_window_size, void *disp_ptr)
{
    Disparity *disp_obj = static_cast<Disparity *>(disp_ptr);
    printf("speckleWindowSize = %d\n", speckle_window_size);
    disp_obj->set_speckleWindowSize(speckle_window_size);
}


void on_speckleRange(int speckle_range, void *disp_ptr)
{
    Disparity *disp_obj = static_cast<Disparity *>(disp_ptr);
    printf("speckleRange = %d\n", speckle_range);
    disp_obj->set_speckleRange(speckle_range);
}


static void saveXYZ(const char* filename, const Mat& mat)
{
    const double max_z = 1.0e4;
    FILE* fp = fopen(filename, "wt");
    for (int y = 0; y < mat.rows; y++)
    {
        for (int x = 0; x < mat.cols; x++)
        {
            Vec3f point = mat.at<Vec3f>(y, x);
            if (fabs(point[2] - max_z) < FLT_EPSILON || fabs(point[2]) > max_z)
                continue;
            fprintf(fp, "%f %f %f\n", point[0], point[1], point[2]);
        }
    }
    fclose(fp);
}


Disparity::Disparity(cv::Size img_size, cv::CommandLineParser &parser) :
    _img_size(img_size), _alg(eStereo::SGBM), _minDisparity(0), _speckleWindowSize(100), _speckleRange(32)
{
    if (parser.has("algorithm"))
    {
        const std::string alg_str = parser.get<std::string>("algorithm");
        _alg = alg_str == "bm" ? eStereo::BM :
            alg_str == "sgbm" ? eStereo::SGBM :
            alg_str == "hh" ? eStereo::HH :
            alg_str == "var" ? eStereo::VAR :
            alg_str == "sgbm3way" ? eStereo::THREEWAY : eStereo::UNKNOWN;
    }
    _numberOfDisparities = parser.get<int>("max-disparity");
    _SADWindowSize = parser.get<int>("blocksize");
    if (parser.has("min-disparity"))
    {
        _minDisparity = parser.get<int>("min-disparity");
        printf("minDisparity = %d\n", _minDisparity);
    }
    if (parser.has("speckle-window-size"))
    {
        _speckleWindowSize = parser.get<int>("speckle-window-size");
        printf("speckleWindowSize = %d\n", _speckleWindowSize);
    }
    if (parser.has("speckle-range"))
    {
        _speckleRange = parser.get<int>("speckle-range");
        printf("speckleRange = %d\n", _speckleRange);
    }
    if (parser.has("i"))
        _intrinsic_filename = parser.get<std::string>("i");
    if (parser.has("e"))
        _extrinsic_filename = parser.get<std::string>("e");
    if (parser.has("o"))
        _disparity_filename = parser.get<std::string>("o");
    if (parser.has("p"))
        _point_cloud_filename = parser.get<std::string>("p");
}


bool Disparity::init()
{
    if (_alg == eStereo::UNKNOWN)
    {
        printf("Command-line parameter error: Unknown stereo algorithm\n\n");
        print_help();
        return false;
    }
    if (_numberOfDisparities < 1 || _numberOfDisparities % 16 != 0 )
    {
        printf("Command-line parameter error: The max disparity (--max-disparity=<...>) must be a positive integer divisible by 16\n");
        print_help();
        return false;
    }
    if (_SADWindowSize < 1 || _SADWindowSize % 2 != 1)
    {
        printf("Command-line parameter error: The block size (--blocksize=<...>) must be a positive odd number\n");
        return false;
    }
    if ((!_intrinsic_filename.empty()) ^ (!_extrinsic_filename.empty()) )
    {
        printf("Command-line parameter error: either both intrinsic and extrinsic parameters must be specified, or none of them (when the stereo pair is already rectified)\n");
        return false;
    }

    if (_extrinsic_filename.empty() && !_point_cloud_filename.empty() )
    {
        printf("Command-line parameter error: extrinsic and intrinsic parameters must be specified to compute the point cloud\n");
        return false;
    }

    printf("img_size = %d, %d\n", _img_size.width, _img_size.height);

    if (!_intrinsic_filename.empty())
    {
        // reading intrinsic parameters
        FileStorage fs(_intrinsic_filename, FileStorage::READ);
        if (!fs.isOpened())
        {
            printf("Failed to open file %s\n", _intrinsic_filename.c_str());
            return false;
        }

        Mat M1, D1, M2, D2;
        fs["M1"] >> M1;
        fs["D1"] >> D1;
        fs["M2"] >> M2;
        fs["D2"] >> D2;

        fs.open(_extrinsic_filename, FileStorage::READ);
        if (!fs.isOpened())
        {
            printf("Failed to open file %s\n", _extrinsic_filename.c_str());
            return false;
        }

        Mat R, T, R1, P1, R2, P2;
        fs["R"] >> R;
        fs["T"] >> T;

        stereoRectify(M1, D1, M2, D2, _img_size, R, T, R1, R2, P1, P2, _Q, CALIB_ZERO_DISPARITY, -1, _img_size, &_roi1, &_roi2);

        initUndistortRectifyMap(M1, D1, R1, P1, _img_size, CV_16SC2, _map11, _map12);
        initUndistortRectifyMap(M2, D2, R2, P2, _img_size, CV_16SC2, _map21, _map22);
    }

    _numberOfDisparities = _numberOfDisparities > 0 ? _numberOfDisparities : ((_img_size.width / 8) + 15) & -16;
    printf("numberOfDisparities = %d (calc = %d)\n", _numberOfDisparities, ((_img_size.width / 8) + 15) & -16);

    switch (_alg)
    {
        case eStereo::BM:
            _pBM = StereoBM::create(16,9);
            _pBM->setROI1(_roi1);
            _pBM->setROI2(_roi2);
            _pBM->setPreFilterCap(31);
            _pBM->setBlockSize(_SADWindowSize > 0 ? _SADWindowSize : 9);
            _pBM->setMinDisparity(_minDisparity); // 0);
            _pBM->setNumDisparities(_numberOfDisparities);
            _pBM->setTextureThreshold(10);
            _pBM->setUniquenessRatio(15);
            _pBM->setSpeckleWindowSize(_speckleWindowSize); // 100);
            _pBM->setSpeckleRange(_speckleRange); // 32);
            _pBM->setDisp12MaxDiff(1);
            break;
        case eStereo::SGBM:
        case eStereo::HH:
        case eStereo::THREEWAY:
            _pSGBM = StereoSGBM::create(0,16,3);
            _pSGBM->setPreFilterCap(63);
            const int sgbmWinSize = _SADWindowSize > 0 ? _SADWindowSize : 3;
            printf("sgbmWinSize = %d\n", sgbmWinSize);
            _pSGBM->setBlockSize(sgbmWinSize);

            int cn = 3; //img1.channels();
            printf("cn = %d\n", cn);

            const int P1 = 8*cn*sgbmWinSize*sgbmWinSize;
            printf("P1 = %d\n", P1);
            _pSGBM->setP1(P1);
            const int P2 = 32*cn*sgbmWinSize*sgbmWinSize;
            printf("P2 = %d\n", P2);
            _pSGBM->setP2(P2);
            _pSGBM->setMinDisparity(_minDisparity); // 0);
            _pSGBM->setNumDisparities(_numberOfDisparities);
            _pSGBM->setUniquenessRatio(10);
            _pSGBM->setSpeckleWindowSize(_speckleWindowSize); // 100);
            _pSGBM->setSpeckleRange(_speckleRange); // 32);
            _pSGBM->setDisp12MaxDiff(1);
            switch (_alg)
            {
                case eStereo::HH:
                    _pSGBM->setMode(StereoSGBM::MODE_HH);
                    break;
                case eStereo::SGBM:
                    _pSGBM->setMode(StereoSGBM::MODE_SGBM);
                    break;
                case eStereo::THREEWAY:
                    _pSGBM->setMode(StereoSGBM::MODE_SGBM_3WAY);
                    break;
            }
            break;
    }

    return true;
}


bool Disparity::show_disparity()
{
    VideoCapture capr(4), capl(2);

    // reduce frame size
    capl.set(CAP_PROP_FRAME_HEIGHT, _img_size.height);
    capl.set(CAP_PROP_FRAME_WIDTH, _img_size.width);
    capr.set(CAP_PROP_FRAME_HEIGHT, _img_size.height);
    capr.set(CAP_PROP_FRAME_WIDTH, _img_size.width);

    namedWindow("Disparity", WINDOW_NORMAL);
    namedWindow("Left", WINDOW_NORMAL);
    createTrackbar("minDisparity" /* + 30"*/, "Disparity", &_minDisparity, 60, on_minDisp, (void *)this);
    createTrackbar("numDisparity", "Disparity", &_numberOfDisparities, 192, on_numDisp, (void *)this);
    createTrackbar("blocksize", "Disparity", &_SADWindowSize, 150, on_blocksize, (void *)this);
    createTrackbar("speckleWindowSize", "Disparity", &_speckleWindowSize, 150, on_speckleWindowSize, (void *)this);
    createTrackbar("speckleRange", "Disparity", &_speckleRange, 150, on_speckleRange, (void *)this);

    on_minDisp(_minDisparity, this);
    on_numDisp(_numberOfDisparities, this);
    on_blocksize(_SADWindowSize, this);
    on_speckleWindowSize(_speckleWindowSize, this);
    on_speckleRange(_speckleRange, this);

    Mat disp, disp_show, disp_compute;
    Mat xyz;
    while (char(waitKey(1)) != 'q')
    {
        // grab raw frames first
        capl.grab();
        capr.grab();
        // decode later so the grabbed frames are less apart in time
        Mat framel, framel_rect, framer, framer_rect;
        capl.retrieve(framel);
        capr.retrieve(framer);

        if (framel.empty() || framer.empty())
            break;

        remap(framel, framel_rect, _map11, _map12, INTER_LINEAR);
        remap(framer, framer_rect, _map21, _map22, INTER_LINEAR);

        int64 t = getTickCount();
        switch (_alg)
        {
            case eStereo::BM:
                _pBM->compute(framel_rect, framer_rect, disp);
                break;
            case eStereo::SGBM:
            case eStereo::HH:
            case eStereo::THREEWAY:
                _pSGBM->compute(framel_rect, framer_rect, disp);
                break;
        }
        t = getTickCount() - t;
        // printf("Time elapsed: %fms\n", t * 1000 / getTickFrequency());

        if (_alg != eStereo::VAR)
        {
            disp.convertTo(disp_show, CV_8U, 255 / (_numberOfDisparities * 16.));
            disp.convertTo(disp_compute, CV_32F, 1.f / 16.f);
        }
        else
        {
            disp.convertTo(disp_show, CV_8U);
        }

        // Draw red rectangle around 40 px wide square area im image
        int xmin = framel.cols / 2 - 20, xmax = framel.cols / 2 + 20, ymin = framel.rows / 2 - 20, ymax = framel.rows / 2 + 20;
        rectangle(framel_rect, Point(xmin, ymin), Point(xmax, ymax), Scalar(0, 0, 255));
        rectangle(disp_show, Point(xmin, ymin), Point(xmax, ymax), Scalar(0, 0, 255));

        reprojectImageTo3D(disp, xyz, _Q, true);

        // Extract depth of 40 px rectangle and print out their mean
        Mat xyz_rect = xyz(Range(ymin, ymax), Range(xmin, xmax));
        // cout << "pointcloud.size() = " << pointcloud.size() << endl;
        Mat z_roi(xyz_rect.size(), CV_32FC1);
        int from_to[] = {2, 0};
        mixChannels(&xyz_rect, 1, &z_roi, 1, from_to, 1);

        // cout << "z_roi.size() = " << z_roi.size() << endl;
        // cout << "Depth: " << mean(z_roi) << " mm" << endl;
        // Scalar mean_out = mean(z_roi);
        // cout << "Depth: " << mean_out << " mm" << endl;
        Scalar mean_out, stddev_out;
        meanStdDev(z_roi, mean_out, stddev_out);
        printf("Depth: %f mm, %f\n", mean_out[0], stddev_out[0]);

        imshow("Disparity", disp_show);
        imshow("Left", framel_rect);
    }

    if (!_disparity_filename.empty())
    {
        printf("Writing the disparity image...");
        imwrite(_disparity_filename, disp_show);
    }

    if (!_point_cloud_filename.empty())
    {
        printf("Storing the point cloud...");
        fflush(stdout);
        saveXYZ(_point_cloud_filename.c_str(), xyz);
        printf("\n");
    }

    return true;
}


int main(int argc, char** argv)
{
    Size img_size(960, 540);

    cv::CommandLineParser parser(argc, argv,
        "{help h||}{algorithm||}{max-disparity|0|}{blocksize|0|}{min-disparity||}{speckle-window-size||}{speckle-range||}{i||}{e||}{o||}{p||}");
    if (parser.has("help"))
    {
        print_help();
        return 0;
    }
    if (!parser.check())
    {
        parser.printErrors();
        return 1;
    }

    Disparity disp(img_size, parser);
    if (!disp.init())
    {
        return -1;
    }
    if (!disp.show_disparity())
    {
        return -1;
    }

    return 0;
}
